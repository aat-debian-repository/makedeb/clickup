# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
#
# From Arch Linux PKGBUILD:
# Contributor: Maarten de Boer <maarten@cloudstek.nl>
# Contributor: Yakumo Saki <yakumo at ziomatrix dot org>
# Contributor: Xuanwo <xuanwo@archlinuxcn.org>
pkgname=clickup
pkgver=3.4.6
pkgrel=0
pkgdesc="Desktop app for clickup.com"
arch=('amd64')
url="https://clickup.com"
license=('custom')
# missing libindicator-gtk2 package, but does not seem to affect basic functionalities
depends=('libgtk-3-0' 'libdbus-glib-1-2' 'libasound2' 'libatopology2' 'libdbusmenu-gtk4' 'libnss3')
makedepends=('sed')
options=('!strip')

_filename="$pkgname-desktop-$pkgver-x86_64.AppImage"

source=("$pkgname-desktop-$pkgver-x86_64.AppImage::https://desktop.clickup.com/linux" "terms.html::https://clickup.com/terms")

sha256sums=('c0cb54c1d8537d24ed10aa11a0dfc4f671820284a549c4354cdf5e7a4a450ae2' '5f862a66f7464e03cc3d16904ce835e06e82ff054e5b0f0315d6edc2392d4b40')

prepare() {
    rm -rf squashfs-root
    chmod +x $_filename
    ./$_filename --appimage-extract

    # Fix directory permissions (read + execute)
    find squashfs-root -type d -exec chmod a+rx {} \;
}

package() {
    cd "${srcdir}/squashfs-root"

    # Rename binary
    mv desktop ClickUp

    # Symlink to /usr/bin
    install -dm0755 "${pkgdir}/usr/bin"
    ln -s /opt/clickup/ClickUp "${pkgdir}/usr/bin/clickup"

    # Desktop entry and icons
    mv desktop.desktop ClickUp.desktop
    sed -i -e "s|Exec=.\+|Exec=/usr/bin/${pkgname} %U|" ClickUp.desktop
    sed -i -e "s|Icon=.\+|Icon=ClickUp|" ClickUp.desktop
    install -Dm0644 ClickUp.desktop -t "${pkgdir}/usr/share/applications/"

    # Terms and licenses
    install -Dm0644 "${srcdir}/terms.html" -t "${pkgdir}/usr/share/doc/${pkgname}/"
    install -Dm0644 LICENSE.electron.txt -t "${pkgdir}/usr/share/doc/${pkgname}/"
    install -Dm0644 LICENSES.chromium.html -t "${pkgdir}/usr/share/doc/${pkgname}/"

    # Icons
    icons=(1024x1024 512x512 256x256 128x128 64x64 48x48 32x32 16x16)

    for size in "${icons[@]}"; do
	mv usr/share/icons/hicolor/${size}/apps/desktop.png usr/share/icons/hicolor/${size}/apps/ClickUp.png
	install -Dm0644 usr/share/icons/hicolor/${size}/apps/ClickUp.png -t "${pkgdir}/usr/share/icons/hicolor/${size}/apps/"
    done

    # AppDir contents
    cd "${srcdir}"
    install -dm0755 "${pkgdir}/opt"
    mv squashfs-root "${pkgdir}/opt/clickup"
    chmod 755 "${pkgdir}/opt/clickup"
    chmod a+rX "${pkgdir}/opt/clickup/"

    # Clean up files
    rm -r "${pkgdir}/opt/clickup/usr/share/"
    rm "${pkgdir}/opt/clickup/AppRun" "${pkgdir}/opt/clickup/ClickUp.desktop"
    rm "${pkgdir}/opt/clickup/desktop.png" "${pkgdir}/opt/clickup/.DirIcon"
    rm "${pkgdir}/opt/clickup/LICENSE.electron.txt" "${pkgdir}/opt/clickup/LICENSES.chromium.html"
}
